export default ({ store, redirect }) => {
	if (store.getters['auth/user'].roles[0].name != 'Developer' && store.getters['auth/user'].roles[0].name != 'Admin' && store.getters['auth/user'].roles[0].name != 'Super Admin') {
		return redirect('/employee_dashboard');
	}
};
