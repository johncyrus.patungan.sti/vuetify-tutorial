import Vue from 'vue'

Vue.prototype.$navs = [
  {
    icon : 'home',
    title : 'Home',
    path : 'home',
    exact : true,
    showNav : false,
    regex : /dashboard.*/i,
    allowed : [],
    submenu : []
  },
  {
    title : 'Developer Tools',
    icon : 'hammer-wrench',
    show : true,
    showNav : false,
    regex : /^[^.]*dt./i,
    allowed : [],
    submenu : [
      {
        text : 'Users',
        path : 'dt.developer-tools.users.index',
        allowed: []
      },
      {
        text : 'Roles',
        path : 'dt.developer-tools.roles.index',
        allowed: []
      },
      {
        text : 'Permission',
        path : 'dt.developer-tools.permissions.index',
        allowed: []
      },
      {
        text : 'Activities',
        path : 'dt.developer-tools.activities.index',
        allowed: []
      },
    ]
  },
  {
    icon : 'cog-outline',
    title : 'Setup',
    path : 'setup.global',
    exact : true,
    showNav : false,
    regex : /^[^.]*dt./i,
    allowed : [],
    submenu : []
  },
]